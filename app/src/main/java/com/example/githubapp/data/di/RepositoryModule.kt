package com.example.githubapp.data.di

import com.example.githubapp.data.api.GitRepoApiService
import com.example.githubapp.data.repository.GitRepoRemoteDataSource
import com.example.githubapp.data.repository.GitRepoRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideGitRemoteDataSourceImpl(gitRepoApiService: GitRepoApiService): GitRepoRemoteDataSource {
        return GitRepoRemoteDataSourceImpl(gitRepoApiService)
    }
}