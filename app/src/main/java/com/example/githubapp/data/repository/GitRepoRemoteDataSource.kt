package com.example.githubapp.data.repository

import com.example.githubapp.data.model.GitRepoResponse
import io.reactivex.rxjava3.core.Single

interface GitRepoRemoteDataSource {

    fun getGitRepositoryData(searchQuery: String): Single<GitRepoResponse>

}