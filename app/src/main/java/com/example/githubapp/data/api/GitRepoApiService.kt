package com.example.githubapp.data.api

import com.example.githubapp.data.model.GitRepoResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GitRepoApiService {

    @GET("/search/repositories")
    fun getBetShopLocationData(
        @Query("q") searchQuery: String
    ): Single<GitRepoResponse>
}