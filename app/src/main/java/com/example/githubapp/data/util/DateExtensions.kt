package com.example.githubapp.data.util

import java.text.SimpleDateFormat
import java.util.*

fun Date.toSimpleString() : String {
    val format = SimpleDateFormat("dd-MM-yyy", Locale.getDefault())
    return format.format(this)
}