package com.example.githubapp.data.repository

import com.example.githubapp.data.api.GitRepoApiService
import com.example.githubapp.data.model.GitRepoResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GitRepoRemoteDataSourceImpl @Inject constructor(
    private val gitRepoApiService: GitRepoApiService
): GitRepoRemoteDataSource {

    override fun getGitRepositoryData(searchQuery: String): Single<GitRepoResponse> {
        return gitRepoApiService.getBetShopLocationData(searchQuery)
    }
}