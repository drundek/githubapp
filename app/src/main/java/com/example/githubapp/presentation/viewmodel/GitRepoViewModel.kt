package com.example.githubapp.presentation.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.githubapp.R
import com.example.githubapp.data.model.Item
import com.example.githubapp.data.repository.GitRepoRemoteDataSource
import com.example.githubapp.data.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class GitRepoViewModel @Inject constructor(
    private val application: Application,
    private val gitRepoRemoteDataSource: GitRepoRemoteDataSource
) : ViewModel() {

    private val _gitRepoListData = MutableLiveData<Resource<List<Item>>>()
    val gitRepoListData: LiveData<Resource<List<Item>>>
        get() = _gitRepoListData

    private val _filteredRepoListData = MutableLiveData<List<Item>>()
    val filteredRepoListData: LiveData<List<Item>>
        get() = _filteredRepoListData

    fun getGitRepositoryData(searchQuery: String) {
        gitRepoRemoteDataSource.getGitRepositoryData(searchQuery)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _gitRepoListData.postValue(Resource.Loading(null))
            }
            .subscribe(
                { gitRepo ->
                    _gitRepoListData.postValue(Resource.Success(gitRepo.items))
                },
                { error ->
                    Log.e("GitRepoViewModel", error.message.toString())
                    _gitRepoListData.postValue(Resource.Error(error.message!!, null))
                }
            )
    }

    fun filterRepositoryListAsc(param: String) {
        when (param) {
            application.getString(R.string.stars) -> {
                _filteredRepoListData.postValue(_gitRepoListData.value?.data?.sortedBy { it.stargazersCount })
            }
            application.getString(R.string.forks_dialog) -> {
                _filteredRepoListData.postValue(_gitRepoListData.value?.data?.sortedBy { it.forksCount })
            }
            application.getString(R.string.last_updated_dialog) -> {
                _filteredRepoListData.postValue(_gitRepoListData.value?.data?.sortedBy {
                    it.updatedAt
                })
            }
        }
    }

    fun filterRepositoryListDesc(param: String) {
        when (param) {
            application.getString(R.string.stars) -> {
                _filteredRepoListData.postValue(_gitRepoListData.value?.data?.sortedByDescending { it.stargazersCount })
            }
            application.getString(R.string.forks_dialog) -> {
                _filteredRepoListData.postValue(_gitRepoListData.value?.data?.sortedByDescending { it.forksCount })
            }
            application.getString(R.string.last_updated_dialog) -> {
                _filteredRepoListData.postValue(_gitRepoListData.value?.data?.sortedByDescending {
                    it.updatedAt
                })
            }
        }
    }
}