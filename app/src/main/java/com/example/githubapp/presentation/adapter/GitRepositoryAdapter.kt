package com.example.githubapp.presentation.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.githubapp.data.model.Item
import com.example.githubapp.databinding.GitRepositoryItemBinding

class GitRepositoryAdapter(
    private var itemList: ArrayList<Item> = arrayListOf(),
    private var onItemClicked: (Item) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<GitRepositoryAdapter.GitRepositoryViewHolder>() {

    inner class GitRepositoryViewHolder(private val binding: GitRepositoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Item, onItemClicked: (Item) -> Unit) {
            binding.apply {
                tvRepoName.text = item.name
                tvAuthorName.text = item.owner.login
                tvWatchersNumber.text = item.watchersCount.toString()
                tvForksNumber.text = item.forksCount.toString()
                tvIssuesNumber.text = item.openIssuesCount.toString()

                Glide.with(ivAuthorImg.context)
                    .load(item.owner.avatarUrl)
                    .circleCrop()
                    .into(ivAuthorImg)

                ivAuthorImg.setOnClickListener {
                    openUserDetailsIntent(context, item.owner.htmlUrl)
                }
                root.setOnClickListener {
                    onItemClicked(item)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitRepositoryViewHolder {
        val binding =
            GitRepositoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GitRepositoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GitRepositoryViewHolder, position: Int) {
        val item = itemList[position]
        holder.bind(item,onItemClicked)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(newData: List<Item>) {
        itemList.clear()
        itemList.addAll(newData)
        notifyDataSetChanged()
    }

    fun openUserDetailsIntent(context: Context, url: String){
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }
}