package com.example.githubapp.presentation.fragment

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.SearchView
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubapp.R
import com.example.githubapp.data.model.Item
import com.example.githubapp.data.util.Status
import com.example.githubapp.data.util.setGone
import com.example.githubapp.data.util.setVisible
import com.example.githubapp.databinding.FragmentMainScreenBinding
import com.example.githubapp.presentation.adapter.GitRepositoryAdapter
import com.example.githubapp.presentation.viewmodel.GitRepoViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class MainScreenFragment : androidx.fragment.app.Fragment() {

    private var _binding: FragmentMainScreenBinding? = null
    private val binding get() = _binding!!
    private lateinit var searchView: SearchView
    private lateinit var gitRepositoryAdapter: GitRepositoryAdapter
    private val viewModel: GitRepoViewModel by hiltNavGraphViewModels(R.id.nav_graph)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainScreenBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        setOnClickListeners()
        observeLiveData()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        observeFilteredList()
    }

    override fun onPause() {
        super.onPause()
        searchView.setOnQueryTextListener(null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        searchView = menu.findItem(R.id.search_view).actionView as SearchView
        searchGitRepositories()
        return super.onCreateOptionsMenu(menu, inflater)
    }

    private fun initRecyclerView() {
        val itemList: ArrayList<Item> = arrayListOf()
        gitRepositoryAdapter = GitRepositoryAdapter(itemList, { item ->
            val repositoryItem = Bundle().apply {
                putParcelable(getString(R.string.repository_item), item)
            }
            findNavController().navigate(
                R.id.action_mainScreenFragment_to_repositoryDetailFragment,
                repositoryItem
            )
        }, requireContext())
        binding.rvRepositoryLis.apply {
            adapter = gitRepositoryAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeLiveData() {
        viewModel.gitRepoListData.observe(viewLifecycleOwner, { gitRepo ->
            when (gitRepo.status) {
                Status.LOADING -> {
                    binding.apply {
                        groupList.setGone()
                        groupHint.setGone()
                        groupLoading.setVisible()
                    }
                }
                Status.SUCCESS -> {
                    binding.apply {
                        groupLoading.setGone()
                        groupHint.setGone()
                        groupList.setVisible()
                    }
                    gitRepo.data?.let { gitRepositoryAdapter.setData(it) }
                }
                Status.ERROR -> {
                    binding.apply {
                        groupLoading.setGone()
                        groupList.setGone()
                        groupHint.setGone()
                    }
                }
            }
        })
    }

    private fun searchGitRepositories() {
        Observable.create<String> { emitter ->
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    if (!emitter.isDisposed) {
                        emitter.onNext(newText)
                    }
                    return false
                }
            })
        }
            .debounce(1000, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ query ->
                viewModel.getGitRepositoryData(query)
            },
                {
                    Log.e("SearchView", "Error: $it")
                },
                {
                    Log.d("SearchView", "Completed")
                })
    }

    private fun setOnClickListeners() {
        binding.btnFilter.setOnClickListener {
            showFilterDialog()
        }
    }

    private fun showFilterDialog() {
        val filterDialog = Dialog(requireContext())
        filterDialog.apply {
            setContentView(R.layout.filter_dialog_layout)
            window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            setCancelable(true)
            show()
        }
        val spinner = filterDialog.findViewById<Spinner>(R.id.filter_spinner)
        var itemSelected = ""
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                itemSelected = adapterView?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        val btnConfirm = filterDialog.findViewById<AppCompatButton>(R.id.btn_confirm_filter)
        btnConfirm.setOnClickListener {
            val radioGroup = filterDialog.findViewById<RadioGroup>(R.id.filter_radio_group)
            val checkedBtn = radioGroup.checkedRadioButtonId
            val filterBy = filterDialog.findViewById<RadioButton>(checkedBtn)
            if (itemSelected == getString(R.string.ascending)) {
                viewModel.filterRepositoryListAsc(filterBy.text.toString())
                observeFilteredList()
            } else {
                viewModel.filterRepositoryListDesc(filterBy.text.toString())
                observeFilteredList()
            }
            filterDialog.dismiss()
        }
    }

    private fun observeFilteredList() {
        viewModel.filteredRepoListData.observe(viewLifecycleOwner, { itemList ->
            gitRepositoryAdapter.setData(itemList)
        })
    }


}