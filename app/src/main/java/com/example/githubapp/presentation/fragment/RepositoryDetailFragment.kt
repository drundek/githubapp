package com.example.githubapp.presentation.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import com.bumptech.glide.Glide
import com.example.githubapp.R
import com.example.githubapp.data.model.Item
import com.example.githubapp.data.util.toSimpleString
import com.example.githubapp.databinding.FragmentRepositoryDetailBinding
import com.example.githubapp.presentation.viewmodel.GitRepoViewModel

class RepositoryDetailFragment : Fragment() {

    private var _binding: FragmentRepositoryDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GitRepoViewModel by hiltNavGraphViewModels(R.id.nav_graph)
    private val repositoryItem: Item by lazy {
        RepositoryDetailFragmentArgs.fromBundle(requireArguments()).repositoryItem
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRepositoryDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindRepoItemDataToView(repositoryItem)
        setOnClickListeners()
    }

    private fun bindRepoItemDataToView(item: Item) {
        binding.apply {
            tvRepoName.text = item.name
            tvAuthorValue.text = item.owner.login
            tvLanguageValue.text = item.language
            tvDateOfCreationValue.text = item.createdAt.toSimpleString()
            tvLastUpdateValue.text = item.updatedAt.toSimpleString()
            tvOwnerIdValue.text = item.owner.id.toString()
            tvOwnerTypeValue.text = item.owner.type

            if (item.owner.siteAdmin) {
                Glide.with(ivOwnerAdminValue.context)
                    .load(R.drawable.ic_true)
                    .into(ivOwnerAdminValue)
            } else {
                Glide.with(ivOwnerAdminValue.context)
                    .load(R.drawable.ic_false)
                    .into(ivOwnerAdminValue)
            }

            Glide.with(ivUserImg.context)
                .load(item.owner.avatarUrl)
                .circleCrop()
                .into(ivUserImg)
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            btnUserDetails.setOnClickListener {
                openUserDetailsIntent(repositoryItem)
            }
            btnRepoDetails.setOnClickListener {
                openRepositoryDetailsIntent(repositoryItem)
            }
        }
    }

    private fun openUserDetailsIntent(item: Item) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(item.owner.htmlUrl)
        requireContext().startActivity(intent)
    }

    private fun openRepositoryDetailsIntent(item: Item) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(item.htmlUrl)
        requireContext().startActivity(intent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}