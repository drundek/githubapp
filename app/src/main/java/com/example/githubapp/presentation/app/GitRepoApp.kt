package com.example.githubapp.presentation.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GitRepoApp: Application() {
}